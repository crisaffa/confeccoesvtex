package br.com.casaamerica.confecVTEX.entities;

import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import io.sentry.Sentry;

public class Produto {

	private String idProduto;
	private String grupo;
	private String descricao;
	private String ean;
	private Double preco;
	private int estoque;
	private String marca;
	private String cor;
	private String modelo;
	private String peso;
	private String altura;
	private String largura;
	private String comprimento;
	private String itemGrade;
	private int posGrade;
	private Double valorPromocao;
	private Date dataLimitePromocao;
	private int qtdPhotos;

	/* Campos para VTEX */
	private String nome;
	private int categoryId;
	private int brandId;
	private int idGrupo;
	private int idNcm;

	public Produto(JSONObject jsonObject) throws ParseException {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		try {
			Long estoque = (Long) jsonObject.get("estoque");
			String ean = String.valueOf(jsonObject.get("ean"));
			this.setEstoque(estoque.intValue());
			this.ean = ean;
			Long qtdPhotos = (Long) jsonObject.get("quantidadeFotos");
			this.setQtdPhotos(qtdPhotos.intValue());
			String grupo = (String) jsonObject.get("grupoId");
			this.setGrupo(grupo);
			Long posGrade = (Long) jsonObject.get("posGrade");
			this.setPosGrade(posGrade.intValue());
			String idProduto = String.valueOf((Long) jsonObject.get("id"));
			System.out.println("id: "+idProduto);
			this.idProduto = idProduto;
			String itemGrade = (String) jsonObject.get("itemGrade");
			this.setItemGrade(itemGrade);				
			if(jsonObject.get("valorPromocao") == null) {
				this.setValorPromocao(0.0);
			} else {
				String aux = jsonObject.get("valorPromocao").toString();
				if(!aux.equals("0")){
					System.out.println("aux: "+aux);
					this.setValorPromocao(Double.parseDouble(aux));
				} else {
					Long valorPromocao = (Long) jsonObject.get("valorPromocao");
					this.setValorPromocao(valorPromocao.doubleValue());
				}			
			}	
			String cor = (String) jsonObject.get("cor");
			this.setCor(cor);
			double precoVenda = (Double) jsonObject.get("precoVenda");
			this.setPreco(precoVenda);
			String marca = (String) jsonObject.get("marca");
			this.setMarca(marca);
			String modelo = (String) jsonObject.get("modelo");
			this.setModelo(modelo);
			String descricao = (String) jsonObject.get("descricao");
			if (descricao.contains("Unisex")) {
				descricao = descricao.replace("Unisex", "Unissex");
			}
			if (descricao.contains("Confortflex")) {
				descricao = descricao.replace("Confortflex", "Comfortflex");
			}
			if (descricao.contains("Cacharel")) {
				descricao = descricao.replace("Cacharel", "Cacharrel");
			}
			this.setDescricao(descricao);
			String peso = (String) jsonObject.get("peso");
			this.setPeso(peso);
			String altura = (String) jsonObject.get("altura");
			this.setAltura(altura);
			String largura = (String) jsonObject.get("largura");
			this.setLargura(largura);
			String comprimento = (String) jsonObject.get("comprimento");
			this.setComprimento(comprimento);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = (String) jsonObject.get("dataLimitePromocao");
			if (strDate != null) {
				Date dataAux = new Date(sdf.parse(strDate).getTime());
				this.setDataLimitePromocao(dataAux);
			}
		} catch(Exception ex) {
			Sentry.capture(ex);
		}
	}

	public int getCategoryId() {
		return categoryId;
	}

	public boolean setCategoryId(ArrayList<Category> categories) {

		String descricao = this.getDescricao();
		String modelo = this.getModelo();
		String marca = this.getMarca();
		String cor = this.getCor();

		int id = 0;
		descricao = ((descricao.replace(marca, "").replace(modelo, "")).replace(cor, "").replace("-", "").toLowerCase())
				.replace(" ", "/");

		if (descricao.contains("confecção")) {
			descricao = descricao.replace("confecção", "confeccao");
		}
		if (descricao.contains("bebê")) {
			descricao = descricao.replace("bebê", "bebe");
		}		
		if (descricao.contains("confecções")) {
			descricao = descricao.replace("confecções", "confeccoes");
		}
		if (descricao.contains("acessórios")) {
			descricao = descricao.replace("acessórios", "acessorios");
		}
		if (descricao.contains("íntimas")) {
			descricao = descricao.replace("íntimas", "intimas");
		}
		if (descricao.contains("infantil") && descricao.contains("masculino")) {
			descricao = descricao.replace("masculino", "menino");
		}
		if (descricao.contains("infantil") && descricao.contains("feminino")) {
			descricao = descricao.replace("feminino", "menina");
		}
		if (descricao.contains("juvenil") && descricao.contains("feminino")) {
			descricao = descricao.replace("feminino", "garota");
		}
		if (descricao.contains("juvenil") && descricao.contains("masculino")) {
			descricao = descricao.replace("masculino", "garoto");
		}
				
		descricao = removerAcentos(descricao);
		
		while (descricao.charAt(descricao.length() - 1) == '/') {
			descricao = descricao.substring(0, descricao.length() - 1);
		}
				
		for (Category category : categories) {
			if(category.getUrl().contains("-")) {
				String url = category.getUrl().replace("-", "/");
				category.setUrl(url);
			}
			if ((category.getUrl()).equals(descricao)) {
				id = category.getId();
			}			
		}
				
		if (id == 0) {		
			System.out.println("id: "+this.getId());
			System.out.println("descricao: "+descricao);
			System.out.println("Category-Id não pode ser 0");
			return false;
		}

		this.categoryId = id;
		
		return true;
	}

	private String parseProdutoNome(Produto produto) {
		String nome = (produto.getDescricao()).replace("Confecções", "").replace("Confecção Praia", "").replace("Acessórios", "").replace("Acessorios", "")
				.replace("Bebê","").replace("Bebe", "").replace("Bijuteria", "").replace("Roupas Intimas", "").replace("Lingerie", "")
				.replace("Roupas Intimas", "").replace("Adulto", "").replace("Infantil", "").replace("Juvenil", "")
				.replace("Unissex", "").replace("Feminino", "").replace("Masculino", "").replace(produto.getMarca(), "")
				.replace(produto.getModelo(), "").replace(produto.getCor(), "").trim();
		
		return nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome() {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		try {					
			String modelo = this.getModelo().replace(this.getCor(), "");
			if (modelo.length() > 0 && modelo.charAt(0) == '-') {
				this.setModelo(modelo.substring(1, modelo.length()));
			}
			if (modelo.length() > 0 && modelo.charAt(modelo.length() - 1) == ' ') {
				this.setModelo(modelo.substring(0, modelo.length() - 1));
			}
			String nome = this.parseProdutoNome(this)+" "+this.getMarca()+" "+modelo+"";
			
			if (nome.contains("Bebecê")) {
				nome = nome.replace("Bebecê", "Bebece");
			}
			if (nome.contains("Lenço")) {
				nome = nome.replace("Lenço", "Lenco");
			}			
			
			nome = nome.replaceAll("\\s+$", "");
			nome = nome.replaceAll("-", "");
			nome = nome.replaceAll("\\s\\s", "");		
			
			nome = removerAcentos(nome);
			
			this.nome = nome;
		} catch(Exception e) {
			Sentry.capture(e);
		}
	}

	public String getId() {
		return idProduto;
	}

	public void setId(String idProduto) {
		this.idProduto = idProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public int getEstoque() {
		return estoque;
	}

	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		if(marca.equals("Confortflex")) {
			marca = "Comfortflex";
		}
		marca = removerAcentos(marca);
		this.marca = marca;
	}

	public String getCor() {
		return cor;
	}
	
	public String removerAcentos(String str) {			
	    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	public void setCor(String cor) {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		try {					
			cor = removerAcentos(cor);
			if(cor.contains("ç")){
				cor = cor.replace("ç", "c"); 
			}
			
			if(cor != null) {
				if(cor.charAt(0) == ' ') {
					cor = cor.substring(1, cor.length());
					this.cor = cor;
				}
				if (cor.length() > 0 && cor.charAt(0) == '-') {
					this.cor = cor.substring(1, cor.length());
					if(getCor().charAt(0) == ' '){
						cor = getCor().substring(1, getCor().length());
						this.cor = cor;
					}
				} else {
					this.cor = cor;	
				}	
			}
		} catch(Exception ex) {
			Sentry.capture(ex);
		}
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {		
		modelo = removerAcentos(modelo);
		if(modelo.contains("ç")){
			modelo = modelo.replace("ç", "c"); 
		}
		this.modelo = modelo;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getLargura() {
		return largura;
	}

	public void setLargura(String largura) {
		this.largura = largura;
	}

	public String getComprimento() {
		return comprimento;
	}

	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}

	public Double getValorPromocao() {
		return valorPromocao;
	}

	public void setValorPromocao(Double valorPromocao) {
		this.valorPromocao = valorPromocao;
	}

	public String getItemGrade() {
		return itemGrade;
	}

	public void setItemGrade(String itemGrade) {
		this.itemGrade = itemGrade;
	}

	public int getPosGrade() {
		return posGrade;
	}

	public void setPosGrade(int posGrade) {
		this.posGrade = posGrade;
	}

	public Date getDataLimitePromocao() {
		return dataLimitePromocao;
	}

	public void setDataLimitePromocao(Date dataLimitePromocao) {
		this.dataLimitePromocao = dataLimitePromocao;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public int getQtdPhotos() {
		return qtdPhotos;
	}

	public void setQtdPhotos(int qtdPhotos) {
		this.qtdPhotos = qtdPhotos;
	}

	public int getIdGrupo() {
		return this.idGrupo;
	}

	public int getIdNcm() {
		return this.idNcm;
	}

	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}

	public void setNcm(int idNcm) {
		this.idNcm = idNcm;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public int getBrandId() {
		return this.brandId;
	}
}
