package br.com.casaamerica.confecVTEX.principal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import br.com.casaamerica.confecVTEX.entities.Brand;
import br.com.casaamerica.confecVTEX.entities.Category;
import br.com.casaamerica.confecVTEX.entities.Produto;
import br.com.casaamerica.confecVTEX.services.InfatAPI;
import br.com.casaamerica.confecVTEX.services.ProdutosAPI;
import br.com.casaamerica.confecVTEX.services.VtexAPI;

public class Principal {

	public static void main(String[] args) throws Exception {
		
		/*Código Lukinha
		if (args.length == 0) {
			System.out.println("Adicionar no arquivo launch.json os argumentos para logar na API!");
		}
		ProdutosAPI produtosAPI = new ProdutosAPI(args[0], args[1]);
		*/
		
		ProdutosAPI produtosAPI = new ProdutosAPI("root", "1q2w3e");
		ArrayList<String> idsGrupos = produtosAPI.getIdsGrupos();
		ArrayList<Produto> produtos = produtosAPI.getProdutos(idsGrupos);	
		ArrayList<Brand> brands = VtexAPI.getAllBrands();	
		ArrayList<Category> categories = VtexAPI.getAllCategories();
		
		/* Remove todas as categorias que nao são da calçados */
		ArrayList<Category> parsedCategories = new ArrayList<Category>();
		for (Category category : categories) {
			String url = category.getUrl();				
			if (url.substring(0, 7).equals("bijuter") || url.substring(0, 7).equals("acessor") || url.substring(0, 7).equals("confecc")
					|| url.substring(0, 7).equals("roupas-") ) {				
				parsedCategories.add(category);
			}
		}
		categories = parsedCategories;				
		
		/* Monta os produtos e descarta os sem modelos */
		ArrayList<Produto> parsedProdutos = new ArrayList<Produto>();
		for (Produto produto : produtos) {			
			if(produto.getModelo() != null) {
				if (produto.getModelo().equals("") || produto.getModelo().equals("-") || produto.getModelo().equals("null")) {
					System.out.println("Sem modelo: Código: " +produto.getId());
				} else {									
					produto.setNome();
					boolean categoryOK = produto.setCategoryId(categories);
					if(categoryOK) {
						parsedProdutos.add(produto);	
					}					
				}	
			}			
		}
		produtos = parsedProdutos;				
		
		/* Ordena por id:posGrade -> facilita na depuração */
		Collections.sort(produtos, new Comparator<Produto>() {
			public int compare(Produto p1, Produto p2) {
				return Integer.parseInt(p1.getId()) - Integer.parseInt(p2.getId());
			}
		});		
		
		/* Cria brands na VTEX e seta os brandIds */
		for (Produto produto : produtos) {				
			boolean hasBrand = false;
			for (Brand brand : brands) {
				if(brand.getName().equals(produto.getMarca())) {
					System.out.println("Marca já existente: " + produto.getMarca());
					hasBrand = true;
					produto.setBrandId(brand.getId());
				}
			}
			
			if (!hasBrand) {				
				int brandId = VtexAPI.createBrandVtex(produto.getMarca());
				produto.setBrandId(brandId);							
				boolean verifyBrand = false;
				while(!verifyBrand) {
					ArrayList<Brand> brands_news = VtexAPI.getAllBrands();
					for (Brand brand : brands_news) {
						if(brand.getName().equals(produto.getMarca())) {
							System.out.println("Criou a marca!");
							brands = brands_news;
							verifyBrand = true;
						}
					}
					System.out.println("verificando a criação da marca: "+produto.getMarca());
					Thread.sleep(30000);
				}													
			}			
		}

		for (Produto produto : produtos) {
			System.out.println("id       : "+produto.getId());
			System.out.println("descrição: "+produto.getDescricao());
			System.out.println("modelo   : "+produto.getModelo());
			System.out.println("cor      : "+produto.getCor());
			System.out.println("categoria: "+produto.getCategoryId());
			System.out.println("marca    : "+produto.getMarca());
			System.out.println("PosGrade : "+produto.getPosGrade());
			System.out.println("ItemGrade: "+produto.getItemGrade());
			System.out.println("Estoque  : "+produto.getEstoque());
			System.out.println("ean      : "+produto.getEan());
			System.out.println("----------------------------------");
		}		
		
		/* Cria produtos na VTEX */		
		for (Produto produto : produtos) {				
			if (!VtexAPI.hasProduto(produto)) {
				VtexAPI.insertProduct(produto);
			}			
		}				
		
		Thread.sleep(20000);
			
		/* Insere skus na VTEX */		
		for (Produto produto : produtos) {								
//			//subir somente as fotos:
//			if(produto.getId().equals("80976107")) {
//				int qtdPhotos = produto.getQtdPhotos();
//				if (qtdPhotos != 0) {
//					for (int i = 1; i <= qtdPhotos; i++) {
//						System.out.println("Subindo foto " + i + " de " + qtdPhotos);
//						VtexAPI.uploadImageSku(produto, i);
//					}
//				}				
//			}
															
			/* Procurar pra ver se tem na VTEX */	
			if (!VtexAPI.hasSku(produto)) {				
				/* Verificar se tem imagem no produto */	
				if(VtexAPI.hasImageSku(produto)){
					/* Insere na VTEX */
					boolean insertOK = VtexAPI.insertSku(produto);
					/* Verificar se inseriu o sku na VTEX */
					if(insertOK) {
						VtexAPI.createFieldColorSku(produto);
						VtexAPI.createFieldSizeSku(produto);
						VtexAPI.insertStockSku(produto);					
						/* Sobe as fotos */													
						int qtdPhotos = produto.getQtdPhotos();
						if (qtdPhotos != 0) {
							for (int i = 1; i <= qtdPhotos; i++) {
								System.out.println("Subindo foto " + i + " de " + qtdPhotos);
								VtexAPI.uploadImageSku(produto, i);
							}
						}													
						Thread.sleep(10000);						
						InfatAPI.sendSkuInfat(produto);
					}					
				}
			} else {
				String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();				
				System.out.println("SKU já existente na VTEX: " + " idVtex: " + id_produto_vtex + produto.getPosGrade());
			}				
		}
		
		System.out.println("agora acabou msm!");
		
	}
	
	

}
