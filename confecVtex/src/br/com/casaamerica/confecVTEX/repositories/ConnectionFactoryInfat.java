package br.com.casaamerica.confecVTEX.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactoryInfat {
	public static Connection getConnection() throws Exception  {
        try {
        	
        	Class.forName("org.postgresql.Driver");	
  
            //return DriverManager.getConnection("jdbc:postgresql://10.11.247.170/infat-test", "postgres", "postgres");
            
            return DriverManager.getConnection("jdbc:postgresql://35.199.83.225/infat", "postgres", "postgres");
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
		
}
