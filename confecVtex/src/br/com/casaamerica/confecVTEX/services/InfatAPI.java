package br.com.casaamerica.confecVTEX.services;

import java.sql.ResultSet;

import br.com.casaamerica.confecVTEX.entities.Produto;
import br.com.casaamerica.confecVTEX.util.QueryManager;
import io.sentry.Sentry;

public class InfatAPI {

  private static final QueryManager qm = QueryManager.getInstance();

  public static void sendSkuInfat(Produto produto) throws Exception {	  
	  Sentry.init("https://o183313.ingest.sentry.io/5227086");
	  
	  try {		 	  

		  System.out.println("entrou aqui para enviar o sku para o Infat!");
		    
		  String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();
		  String linkImage = VtexAPI.getImageUrl(id_produto_vtex + produto.getPosGrade());
		  String modelo = produto.getModelo().replace(produto.getCor(), "");
		    
		  if (modelo.length() > 0 && modelo.charAt(0) == '-') {
		    	modelo = modelo.substring(1, modelo.length());
		  }
		
		  /* Postgres */
		  int idGrupo = 0;
		  int idNcm = 0;
		  ResultSet rs = qm.getIdGrupoAndNcm(produto, 0);
		  while (rs.next()) {
			  idGrupo = rs.getInt("id_grupo");
			  idNcm = rs.getInt("id_ncm");
		  }
		
		  produto.setIdGrupo(idGrupo);
		  produto.setNcm(idNcm);
		
		  /* Infat */
		  ResultSet rs2 = qm.getIdProduto(produto, 1);
		  int idProduto = 0;
		  while (rs2.next()) {
			  idProduto = rs2.getInt("id_produto");
		  }
		
		  if (idProduto == 0) {
			  qm.insertProduto(id_produto_vtex, produto, linkImage, 1);
			  System.out.println("Sku inserido com sucesso no Infat: " + produto.getIdGrupo() + produto.getPosGrade());
		  } else {
			  System.out.println("Sku já tinha no Infat: " + id_produto_vtex + produto.getPosGrade());
		  }
	  } catch(Exception ex) {
		  Sentry.capture(ex);
	  }
  }
}