package br.com.casaamerica.confecVTEX.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.casaamerica.confecVTEX.entities.Brand;
import br.com.casaamerica.confecVTEX.entities.Category;
import br.com.casaamerica.confecVTEX.entities.Produto;
import br.com.casaamerica.confecVTEX.util.Request;
import br.com.casaamerica.confecVTEX.util.SoapRequest;
import io.sentry.Sentry;

public class VtexAPI {

	private static final String brandsUrl = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pvt/brand/list";	
	private static final String categoriesUrl = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pub/category/tree/5/";
	private static final String skuUrl = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pvt/sku/stockkeepingunitbyid/";
	private static final String produtoUrl = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pvt/products/ProductGet/";
	private static final String postSkuUrl = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";
	private static final String postCorUrl = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";
	private static final String postSizeUrl = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";
	private static final String postProdutoUrl = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";
	private static final String stockUrl = "http://casaamerica.vtexcommercestable.com.br/api/logistics/pvt/inventory/warehouseitems/setbalance";
	private static final String imageUploadUrl = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";
	private static final String getImageUrl = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pvt/sku/stockkeepingunitbyid/";
	//private static final String linkStorage = "https://storage.googleapis.com/hivelabs-storage/produtos-storage-staging";//homologação
	private static final String linkStorage = "https://storage.googleapis.com/hivelabs-storage/produtos-storage";//produção
	private static final String idLoja = "604101";
	

	public static ArrayList<Brand> getAllBrands() throws Exception {

		System.out.println("entrou aqui pra pegar todas as marcas inseridas na Vtex!");

		ArrayList<Brand> brands = new ArrayList<Brand>();
		StringBuffer buffer = Request.httpGet(brandsUrl, 1, "", "");
		JSONParser parser = new JSONParser();
		JSONArray jsonArray = (JSONArray) parser.parse(buffer.toString());

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.get(i);
			String name = (String) jsonObject.get("name");			
			Long id = (Long) jsonObject.get("id");
			Integer idInt = (int) (long) id;
			Brand brand = new Brand();
			brand.setId(idInt);
			brand.setName(name);			
			brands.add(brand);
		}
		return brands;
	}
	
	public static boolean hasSku(Produto produto) throws Exception {
		
		System.out.println("entrou aqui pra ver se existe o SKU na Vtex!");

		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();
		String url = skuUrl + id_produto_vtex + produto.getPosGrade();
		
		System.out.println("sku: " + id_produto_vtex + produto.getPosGrade());
		String sku = id_produto_vtex + produto.getPosGrade();
		
		StringBuffer buffer = Request.httpGet(url, 1, "", "");
		
		System.out.println("buffer: "+buffer);
		
		if (buffer.toString().contains("SKU não encontrado")) {
			return false;
		}

		return true;
	}
	
	public static boolean hasImageSku(Produto produto) throws Exception {
		
		System.out.println("entrou aqui pra ver se existe Imagem no produto!");

		if(produto.getQtdPhotos() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean uploadImageSku(Produto produto, int numberPhoto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		System.out.println("entrou aqui para carregar uma imagem SKU na vtex!");

		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();
		
		String linkPhoto = linkStorage + "/" + idLoja + "/" + produto.getId() + "/" + numberPhoto + ".png";	
		System.out.println("linkPhoto: "+linkPhoto);
		
		StringEntity request = SoapRequest.getUploadImageRequest(id_produto_vtex, produto, linkPhoto);
		request.setContentEncoding("UTF-8");
		request.setContentType("text/xml");
		String soapAction = "http://tempuri.org/IService/ImageServiceInsertUpdate";
		HttpResponse response = Request.httpPost(imageUploadUrl, request, soapAction);
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("response: "+response);
			System.out.println("Falha ao subir imagem para vtex");
			Sentry.capture(response.toString());
			return false;			
		} 
		return true;
	}

	public static String getImageUrl(String sku) throws Exception {

		System.out.println("entrou para pegar o link da imagem!");

		String url = getImageUrl + sku;
		StringBuffer response = Request.httpGet(url, 1, "", "");

		String imageUrl = "";
		JSONParser parser = new JSONParser();
		try {
			if (!(response.toString().compareTo("Cannot fill the SkuDocument from database. Sku name:sem sku") == 0)) {
				JSONArray results = (JSONArray) parser.parse("[" + response.toString() + "]");
				for (int i = 0; i < results.size(); i++) {
					JSONObject jsonObject = (JSONObject) results.get(i);
					JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("Images") + "");
					for (int j = 0; j < results2.size(); j++) {
						JSONObject jsonObject2 = (JSONObject) results2.get(j);
						imageUrl = (String) jsonObject2.get("ImageUrl");
						return imageUrl;
					}
				}
				return imageUrl;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "/";
		}
		return "/";
	}

	public static ArrayList<String> returnListIdsProducts(String data) {

		ArrayList<Integer> positionAspas = new ArrayList<Integer>();
		ArrayList<String> listIdsProducts = new ArrayList<String>();

		for (int j = 0; j < data.toString().length(); j++) {
			if (data.toString().charAt(j) == '"') {
				positionAspas.add(j);
			}
		}

		int tam = positionAspas.size();
		int j = 0;
		while (tam != 0) {
			String st = data.toString().substring(positionAspas.get(j) + 1, positionAspas.get(j + 1));
			listIdsProducts.add(st);
			j += 2;
			tam -= 2;
		}
		return listIdsProducts;
	}

	public static boolean hasProduto(Produto produto) throws Exception {

		String urlProd = produtoUrl + produto.getId();
		StringBuffer buffer = Request.httpGet(urlProd, 1, "", "");
		if (buffer.toString().equals("null")) {
			return false;
		} else {
			System.out.println("Produto ja existente: "+produto.getNome()+" "+produto.getCor()+" - " + produto.getId() +" - "+produto.getItemGrade()+"");
			return true;
		}
	}

	public static boolean insertSku(Produto produto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();
		
		System.out.println("Inserindo novo sku na VTEX: " + id_produto_vtex + produto.getPosGrade());
		StringEntity req = SoapRequest.getSoapRequest(produto, id_produto_vtex);
		String soapAction = "http://tempuri.org/IService/StockKeepingUnitInsertUpdate";
		HttpResponse res = Request.httpPost(postSkuUrl, req, soapAction);

		if (res.getStatusLine().getStatusCode() != 200) {		
			System.out.println("Problema ao inserir SKU na VTEX!");
			System.out.println("res: "+res.toString());
			Sentry.capture(res.toString());
			return false;
		}
		return true;
	}

	public static boolean createFieldColorSku(Produto produto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		System.out.println("entrou aqui para criar o campo COR no sku na vtex!");

		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();		
		
		String soapAction = "http://tempuri.org/IService/StockKeepingUnitEspecificationInsert";
		StringEntity req = SoapRequest.getPostCorReq(id_produto_vtex, produto);					
		HttpResponse response = Request.httpPost(postCorUrl, req, soapAction);
		
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Problema ao inserir cor!");
			System.out.println("code: "+response.getStatusLine().getStatusCode());
			System.out.println("response: "+response.toString());
			Sentry.capture(response.toString());
			return false;
		} 		
		return true;
	}

	public static boolean createFieldSizeSku(Produto produto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		System.out.println("entrou aqui para criar o campo TAMANHO no sku na vtex!");
		
		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();

		String soapAction = "http://tempuri.org/IService/StockKeepingUnitEspecificationInsert";
		StringEntity request = SoapRequest.getPostSizeReq(id_produto_vtex, produto);
		HttpResponse response = Request.httpPost(postSizeUrl, request, soapAction);

		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Problema ao inserir tamanho!");
			System.out.println("code: "+response.getStatusLine().getStatusCode());
			System.out.println("response: "+response.toString());
			Sentry.capture(response.toString());
			return false;
		} 
		return true;
	}

	public static boolean insertStockSku(Produto produto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		System.out.println("entrou aqui para inserir o ESTOQUE do sku na vtex!");
		
		String id_produto_vtex = produto.getId().length() == 8 ? (produto.getId()).substring(0, produto.getId().length()-1) : produto.getId();

		String request = "[{'wareHouseId': '1_1', 'itemId': '" + id_produto_vtex + produto.getPosGrade() + "', 'quantity': "
				+ produto.getEstoque() + "}]";

		StringEntity req = new StringEntity(request);
		req.setContentEncoding("UTF-8");
		req.setContentType("application/json");
		HttpResponse response = Request.httpPostRest(stockUrl, req);

		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Problema ao inserir estoque!");
			System.out.println("code: "+response.getStatusLine().getStatusCode());
			System.out.println("response: "+response.toString());
			Sentry.capture(response.toString());
			return false;
		}
		return true;
	}

	public static ArrayList<Category> getAllCategories() throws ClientProtocolException, IOException, ParseException {

		System.out.println("entrou aqui pra pegar todas as categorias inseridas na Vtex!");

		ArrayList<Category> listCategs = new ArrayList<Category>();

		StringBuffer buffer = Request.httpGet(categoriesUrl, 1, "", "");

		JSONParser parser = new JSONParser();
		JSONArray results = (JSONArray) parser.parse(buffer.toString());
		for (int i = 0; i < results.size(); i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);
			Object children = jsonObject.get("children");
			if (!children.toString().equals("[]")) {
				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("children") + "");
				for (int j = 0; j < results2.size(); j++) {
					JSONObject jsonObject2 = (JSONObject) results2.get(j);
					Object children2 = jsonObject2.get("children");
					if (!children2.toString().equals("[]")) {
						JSONArray results3 = (JSONArray) parser.parse("" + jsonObject2.get("children") + "");
						for (int k = 0; k < results3.size(); k++) {
							JSONObject jsonObject3 = (JSONObject) results3.get(k);
							Object children3 = jsonObject3.get("children");
							if (!children3.toString().equals("[]")) {
								JSONArray results4 = (JSONArray) parser.parse("" + jsonObject3.get("children") + "");
								for (int l = 0; l < results4.size(); l++) {
									JSONObject jsonObject4 = (JSONObject) results4.get(l);
									Object children4 = jsonObject4.get("children");
									if (!children4.toString().equals("[]")) {
										JSONArray results5 = (JSONArray) parser.parse("" + jsonObject4.get("children") + "");
										for (int m = 0; m < results5.size(); m++) {
											JSONObject jsonObject5 = (JSONObject) results5.get(m);
											Object children5 = jsonObject5.get("children");
											if (!children5.toString().equals("[]")) {
												JSONArray results6 = (JSONArray) parser.parse("" + jsonObject5.get("children") + "");
												for (int n = 0; n < results6.size(); n++) {
													JSONObject jsonObject6 = (JSONObject) results6.get(n);
													Object children6 = jsonObject6.get("children");
													if (!children6.toString().equals("[]")) {
														//ultimo nivel!
													} else {
														String url = (String) jsonObject6.get("url");
														Long id = (Long) jsonObject6.get("id");
														int idInt = (int) (long) id;
														Category categ = new Category();
														categ.setId(idInt);
														categ.setUrl(url.substring(46, url.length()));
														listCategs.add(categ);
													}
												}
											} else {
												String url = (String) jsonObject5.get("url");
												Long id = (Long) jsonObject5.get("id");
												int idInt = (int) (long) id;
												Category categ = new Category();
												categ.setId(idInt);
												categ.setUrl(url.substring(46, url.length()));
												listCategs.add(categ);
											}
										}
									} else {
										String url = (String) jsonObject4.get("url");
										Long id = (Long) jsonObject4.get("id");
										int idInt = (int) (long) id;
										Category categ = new Category();
										categ.setId(idInt);
										categ.setUrl(url.substring(46, url.length()));
										listCategs.add(categ);
									}
								}
							} else {
								String url = (String) jsonObject3.get("url");
								Long id = (Long) jsonObject3.get("id");
								int idInt = (int) (long) id;
								Category categ = new Category();
								categ.setId(idInt);
								categ.setUrl(url.substring(46, url.length()));
								listCategs.add(categ);
							}
						}
					} else {
						String url = (String) jsonObject2.get("url");
						Long id = (Long) jsonObject2.get("id");
						int idInt = (int) (long) id;
						Category categ = new Category();
						categ.setId(idInt);
						categ.setUrl(url.substring(46, url.length()));
						listCategs.add(categ);
					}
				}
			}
		}
		
		return listCategs;
	}

	public static void insertProduct(Produto produto) throws Exception {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		System.out.println("Inserindo novo produto na VTEX: "+produto.getNome()+" "+produto.getCor()+" - "+produto.getId()+ " - "+produto.getItemGrade());
		
		String soapAction = "http://tempuri.org/IService/ProductInsertUpdate";
		StringEntity request = SoapRequest.getPostProdutoReq(produto);
		HttpResponse response = Request.httpPost(postProdutoUrl, request, soapAction);
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("response: "+response);
			System.out.println("Problema ao inserir produto/marca na VTEX (insertProduct)");
			Sentry.capture(response.toString());
		} else {
			System.out.println("Produto inserido:" + produto.getNome());	
		}			
	}

	public static int createBrandVtex(String marca) throws Exception {

		System.out.println("Criando marca: " + marca);

		String url = "http://webservice-casaamerica.vtexcommerce.com.br/service.svc?wsdl";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		String temp = Normalizer
				.normalize("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
						+ "xmlns:tem='http://tempuri.org/' "
						+ "xmlns:vtex='http://schemas.datacontract.org/2004/07/Vtex.Commerce.WebApps.AdminWcfService.Contracts'> "
						+ "<soapenv:Header/>\r\n" + "  <soapenv:Body>\r\n" + "      <tem:BrandInsertUpdate>\r\n"
						+ "          <tem:brand>\r\n" + "			 <!--descricao da marca-->\r\n" + "          <vtex:Description>Marca "
						+ marca + "</vtex:Description>\r\n" + "          <!--a marca está ativa-->\r\n"
						+ "          <vtex:IsActive>true</vtex:IsActive>\r\n" + "          <!--palavras chave da marca-->\r\n"
						+ "          <vtex:Keywords>" + marca + " Keywords</vtex:Keywords>\r\n"
						+ "          <!--nome da marca-->\r\n" + "			 <vtex:Name>" + marca + "</vtex:Name>\r\n"
						+ "          <!--titulo da marca-->\r\n" + "          <vtex:Title>" + marca
						+ " | Casa America</vtex:Title>\r\n" + "			 </tem:brand>\r\n" + "      </tem:BrandInsertUpdate>\r\n"
						+ "  " + "	</soapenv:Body>\r\n" + "</soapenv:Envelope>", Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}]", "");

		StringEntity stringEntity = new StringEntity(temp);
		stringEntity.setContentEncoding("UTF-8");
		stringEntity.setContentType("text/xml");
		post.setEntity(stringEntity);

		// add header
		post.addHeader("Authorization",
				"Basic dnRleGFwcGtleS1jYXNhYW1lcmljYS1BQkhZRVo6UUFYR1pSVllCV1hERUVSQlVDTlVQUllETEhMWElWUkFYUFFFU1RZWFNLR0dDSkxXUVVYVEFHTVpDVVVTRlhQRkVYR1BPTklCRVBDRUtSQUNXUEhPWVlaUkNSQlhZR1NVVkJDRkNXSENNV1BLRUlPS1VTV1FQQkhHWFBNWExLQ1k="); // PRODUCAO
		post.addHeader("Host", "webservice-casaamerica.vtexcommerce.com.br:8080");
		post.addHeader("SOAPAction", "http://tempuri.org/IService/BrandInsertUpdate");
		post.addHeader("User-Agent", "Jakarta Commons-HttpClient/3.0.1");
		post.addHeader("content-type", "text/xml;charset=UTF-8");
		post.addHeader("Connection", "close");

		HttpResponse response = client.execute(post);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new Exception();
		}

		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		String[] aux = (result.toString()).split("<a:Id>");
		String[] aux2 = (aux[1]).split("</a:Id>");
		int idBrand = Integer.parseInt(aux2[0]);

		return idBrand;
	}		

}
