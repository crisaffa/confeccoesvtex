package br.com.casaamerica.confecVTEX.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.casaamerica.confecVTEX.entities.Produto;
import br.com.casaamerica.confecVTEX.util.Request;
import io.sentry.Sentry;

public class ProdutosAPI {

	private String token = "";
	private String user = "";

	private final String loginUrl = "http://35.199.122.203:5099/api/authenticate"; //producao
	//private final String loginUrl = "http://201.159.154.204:5045/api/authenticate"; //homologacao
	private final String gruposIdsUrl = "http://35.199.122.203:5099/api/grupos/604101"; //producao
	//private final String gruposIdsUrl = "http://201.159.154.204:5045/api/grupos/604101"; //homologacao
	private final String produtosUrl = "http://35.199.122.203:5099/api/produtos/mongo/604101/"; //producao	
	//private final String produtosUrl = "http://201.159.154.204:5045/api/produtos/mongo/604101/"; //homologacao

	public ProdutosAPI(String login, String senha) throws IOException, ParseException {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		try {
			String token = this.login(login, senha);
			this.token = token;
			this.user = login;
		} catch (Exception e) {
            Sentry.capture(e);
			System.out.println(e.getMessage());			
			System.exit(1);
		}
	}

	private String login(String login, String senha) throws IOException, ParseException {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		
		URL url = new URL(loginUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();		
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String jsonInputString = "{ \"login\": \"" + login + "\", \"senha\": \"" + senha + "\" }";		
		try (OutputStream os = con.getOutputStream()) {
			byte[] input = jsonInputString.getBytes("utf-8");
			os.write(input, 0, input.length);
		} catch(Exception ex) {
			Sentry.capture(ex);
		}
		InputStream is = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(is, "utf-8");
		BufferedReader br = new BufferedReader(isr);
		StringBuilder response = new StringBuilder();
		String responseLine = null;
		while ((responseLine = br.readLine()) != null) {
			response.append(responseLine.trim());
		}		
		JSONParser parser = new JSONParser();
		JSONObject res = (JSONObject) parser.parse(response.toString());
		JSONObject user = (JSONObject) res.get("usuario");		
		return (String) user.get("token");
	}

	public ArrayList<String> getIdsGrupos() throws ClientProtocolException, IOException, ParseException {		
		System.out.println("entrou aqui no getIdsGrupos!");		
		
		ArrayList<String> idsGrupos = new ArrayList<String>();		
		StringBuffer buffer = Request.httpGet(gruposIdsUrl, 2, this.token, this.user);		
		JSONParser parser = new JSONParser();
		JSONArray results = (JSONArray) parser.parse("[" + buffer.toString() + "]");		
		for (int i = 0; i < results.size(); i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);
			JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("grupos") + "");
			for (int j = 0; j < results2.size(); j++) {
				JSONObject jsonObject2 = (JSONObject) results2.get(j);				
				String _id = (String) jsonObject2.get("_id");		
				idsGrupos.add(_id);
			}
		}				
		return idsGrupos;
	}

	public ArrayList<Produto> getProdutos(ArrayList<String> gruposId)
			throws ClientProtocolException, IOException, ParseException, java.text.ParseException {			
		System.out.println("entrou aqui no getProdutos!");		
		
		ArrayList<Produto> listaProdutos = new ArrayList<Produto>();
		for (String grupoId : gruposId) {			
			String url = produtosUrl + grupoId;						
			StringBuffer buffer = Request.httpGet(url, 2, token, user);			
			JSONParser parser = new JSONParser();
			JSONArray results = (JSONArray) parser.parse("[" + buffer.toString() + "]");			
			for (int j = 0; j < results.size(); j++) {
				JSONObject jsonObject = (JSONObject) results.get(j);
				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("produtos") + "");
				for (int k = 0; k < results2.size(); k++) {
					JSONObject jsonObject2 = (JSONObject) results2.get(k);
					String status = (String) jsonObject2.get("status");
					Long estoque = (Long) jsonObject2.get("estoque");			
					if (status.equals("Ativo") && estoque.intValue() > 0) {							
						Produto produto = new Produto(jsonObject2);												
						//if(produto.getId().equals("80976507")) {
							listaProdutos.add(produto);
						//}						
					}
				}
			}
		}
		return listaProdutos;
	}
}
