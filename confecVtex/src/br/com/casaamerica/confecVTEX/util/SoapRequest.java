package br.com.casaamerica.confecVTEX.util;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;

import org.apache.http.entity.StringEntity;

import br.com.casaamerica.confecVTEX.entities.Produto;

public class SoapRequest {

  public static StringEntity getSoapRequest(Produto produto, String id_vtex) throws UnsupportedEncodingException {
    String req = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
        + "xmlns:tem=\"http://tempuri.org/\" "
        + "xmlns:vtex=\"http://schemas.datacontract.org/2004/07/Vtex.Commerce.WebApps.AdminWcfService.Contracts\">"
        + "<soapenv:Header/> <soapenv:Body> <tem:StockKeepingUnitInsertUpdate><tem:stockKeepingUnitVO>"
        + "<vtex:CommercialConditionId>1</vtex:CommercialConditionId><vtex:CubicWeight>1</vtex:CubicWeight>"
        + "<vtex:Height>" + produto.getAltura() + "</vtex:Height><vtex:Id>" + id_vtex + produto.getPosGrade()
        + "</vtex:Id><vtex:IsActive>false</vtex:IsActive>"
        + "<vtex:IsAvaiable>true</vtex:IsAvaiable><vtex:IsKit>false</vtex:IsKit><vtex:Length>"
        + produto.getComprimento() + "</vtex:Length><vtex:ListPrice>" + produto.getPreco() + "</vtex:ListPrice>"
        + "<vtex:ModalId>1</vtex:ModalId>" + "<vtex:Name>"+produto.getItemGrade()+"</vtex:Name>" + "<vtex:Price>"
        + produto.getPreco() + "</vtex:Price>" + "<vtex:ProductId>" + produto.getId()
        + "</vtex:ProductId><vtex:RefId>5-" + produto.getId() + "-" + produto.getItemGrade() + "-"
        + produto.getPosGrade() + "</vtex:RefId>" + "<vtex:RewardValue>0</vtex:RewardValue>"
        + "<vtex:StockKeepingUnitEans><vtex:StockKeepingUnitEanDTO><vtex:Ean>" + produto.getEan() + "</vtex:Ean>"
        + "</vtex:StockKeepingUnitEanDTO></vtex:StockKeepingUnitEans>" + "<vtex:UnitMultiplier>1</vtex:UnitMultiplier>"
        + "<vtex:WeightKg>" + produto.getPeso() + "</vtex:WeightKg>" + "<vtex:Width>" + produto.getLargura()
        + "</vtex:Width></tem:stockKeepingUnitVO></tem:StockKeepingUnitInsertUpdate>"
        + "</soapenv:Body></soapenv:Envelope>"; 
    
    System.out.println("req: "+req);    
    
    StringEntity stringEntity = new StringEntity(req);
    stringEntity.setContentEncoding("UTF-8");
    stringEntity.setContentType("text/xml");
    return stringEntity;
  }

  public static StringEntity getPostProdutoReq(Produto produto) throws UnsupportedEncodingException {
	  int idDepartment = 0;
	  if(produto.getDescricao().contains("Confecções")) {
		  idDepartment = 1000307;		  
	  } else if(produto.getDescricao().contains("Confecção Praia")) {
		  idDepartment = 1000288;
	  } else if(produto.getDescricao().contains("Roupas")) {
		  idDepartment = 1000417;
	  } else if(produto.getDescricao().contains("Bijuteria")) {
		  idDepartment = 1000266;
	  }else {
		  idDepartment = 1000267;
	  }       
    
    String req = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        + "xmlns:tem='http://tempuri.org/' "
        + "xmlns:vtex='http://schemas.datacontract.org/2004/07/Vtex.Commerce.WebApps.AdminWcfService.Contracts' "
        + "xmlns:arr='http://schemas.microsoft.com/2003/10/Serialization/Arrays'>" + "<soapenv:Header/>"
        + "<soapenv:Body><tem:ProductInsertUpdate><tem:productVO> " + "<vtex:BrandId>" + produto.getBrandId()
        + "</vtex:BrandId><vtex:CategoryId>" + produto.getCategoryId() + "</vtex:CategoryId><vtex:DepartmentId>"
        + idDepartment + "</vtex:DepartmentId><vtex:Id>" + produto.getId()
        + "</vtex:Id><vtex:IsActive>false</vtex:IsActive>"
        + "<vtex:IsVisible>true</vtex:IsVisible><vtex:KeyWords></vtex:KeyWords><vtex:ListStoreId>"
        + " <arr:int>1</arr:int></vtex:ListStoreId><vtex:Name>"+produto.getNome()+" "+produto.getCor()+"</vtex:Name><vtex:RefId>5-"        
        + produto.getId() + "-" + produto.getItemGrade() + "-" + produto.getPosGrade() + "</vtex:RefId>"        
        + "</tem:productVO>" + "</tem:ProductInsertUpdate></soapenv:Body>" + "</soapenv:Envelope>";
    
    System.out.println("req: "+req);
    
    StringEntity stringEntity = new StringEntity(req);
    stringEntity.setContentEncoding("UTF-8");
    stringEntity.setContentType("text/xml");
    return stringEntity;
  }

  public static StringEntity getPostCorReq(String id_vtex, Produto produto) throws UnsupportedEncodingException {	 
    String req = Normalizer.normalize("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        + "xmlns:tem='http://tempuri.org/' "+"xmlns:arr='http://schemas.microsoft.com/2003/10/Serialization/Arrays'>"
        + "<soapenv:Header/>"+"<soapenv:Body>"+"<tem:StockKeepingUnitEspecificationInsert>"
        + "<!--number, identificador do SKU dono do campo-->"+"<tem:idSku>"+id_vtex+ produto.getPosGrade()+""
        + "</tem:idSku>"+"<!--string, identificador do campo, nome do campo-->"
        + "<tem:fieldName>Cor</tem:fieldName>"+"<!--array, lista de valores dos campos-->"
        + "<tem:fieldValues>"+"<!--string, valor de campo-->"+"<arr:string>"+produto.getCor()+"</arr:string>"
        + "</tem:fieldValues>"+ "</tem:StockKeepingUnitEspecificationInsert>"+"</soapenv:Body>"+"</soapenv:Envelope>",
        Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        
    StringEntity stringEntity = new StringEntity(req);
    stringEntity.setContentEncoding("UTF-8");
    stringEntity.setContentType("text/xml");
    return stringEntity;
  }

  public static StringEntity getPostSizeReq(String id_vtex, Produto produto) throws UnsupportedEncodingException {
    String req = Normalizer.normalize("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        + "xmlns:tem='http://tempuri.org/' " + "xmlns:arr='http://schemas.microsoft.com/2003/10/Serialization/Arrays'>"
        + "<soapenv:Header/>" + "  <soapenv:Body>" + "      <tem:StockKeepingUnitEspecificationInsert>"
        + "			 <!--number, identificador do SKU dono do campo-->" + "          <tem:idSku>" + id_vtex
        + produto.getPosGrade() + "</tem:idSku>" + "          <!--string, identificador do campo, nome do campo-->"
        + "          <tem:fieldName>Tamanho</tem:fieldName>" + "          <!--array, lista de valores dos campos-->"
        + "          <tem:fieldValues>" + "          <!--string, valor de campo-->" + "          <arr:string>"
        + produto.getItemGrade() + "</arr:string>" + "          </tem:fieldValues>"
        + "          </tem:StockKeepingUnitEspecificationInsert>" + "	</soapenv:Body>" + "</soapenv:Envelope>",
        Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    StringEntity stringEntity = new StringEntity(req);
    stringEntity.setContentEncoding("UTF-8");
    stringEntity.setContentType("text/xml");
    return stringEntity;
  }

  public static StringEntity getUploadImageRequest(String id_vtex, Produto produto, String linkPhoto)
      throws UnsupportedEncodingException {	
	  String nome = "";
	  if(produto.getNome().contains(".")) {
		  nome = produto.getNome().replace(".", "");
	  } else {
		  nome = produto.getNome();
	  }
	  
    String req = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        + "xmlns:tem='http://tempuri.org/'>" + "<soapenv:Header/>" + "<soapenv:Body>" + "<tem:ImageServiceInsertUpdate>"
        + "<tem:urlImage>" + linkPhoto + "</tem:urlImage><tem:imageName>"+nome+" "+produto.getCor()
        + "</tem:imageName><tem:stockKeepingUnitId>"+id_vtex+produto.getPosGrade()
        + "</tem:stockKeepingUnitId>" + "</tem:ImageServiceInsertUpdate></soapenv:Body></soapenv:Envelope>";
    
    StringEntity stringEntity = new StringEntity(req);
    stringEntity.setContentEncoding("UTF-8");
    stringEntity.setContentType("text/xml");
    return stringEntity;
  }

}
