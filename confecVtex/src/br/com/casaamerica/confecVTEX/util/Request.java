package br.com.casaamerica.confecVTEX.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.parser.ParseException;

import io.sentry.Sentry;

public class Request {

	public static final String appKey = "vtexappkey-casaamerica-ABHYEZ";
	public static final String appToken = "QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTY"
			+ "XSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY";

	public static StringBuffer httpGet(String url, int type, String token, String user)
			throws ClientProtocolException, IOException, ParseException {
		HttpGet get = new HttpGet(url);
		get = setHeaders(get, type, token, user); // headers para vtex
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(get);
		if (response.getStatusLine().getStatusCode() != 200) {
			String resposta = "[tratarResposta COMTELE] Response Code : " + response.getStatusLine().getStatusCode();
			resposta += "\nReason : " + response.getStatusLine().getReasonPhrase();
			System.out.println(resposta);
		}
		StringBuffer buffer = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			buffer.append(line);
		}

		return buffer;
	}

	public static HttpResponse httpPost(String url, StringEntity request, String soapAction)
			throws ClientProtocolException, IOException {
		Sentry.init("https://o183313.ingest.sentry.io/5227086");
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		post = Request.setPostHeaders(post, soapAction);
		post.setEntity(request);
		try {
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

	        StringBuffer result = new StringBuffer();
	        String line = "";
	        while ((line = rd.readLine()) != null) {
	        	result.append(line);
	        }	       	       
	        
	        System.out.println("result: "+result);
	        
	        return response;
		} catch (Exception e) {
			System.out.println("deu erro aqui: "+e.getCause().toString());
			Sentry.capture(e);
			return null;
		}

	}

	public static HttpResponse httpPostRest(String url, StringEntity request)
			throws ClientProtocolException, IOException {		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		post = Request.setPostRestHeaders(post);
		post.setEntity(request);
		HttpResponse response = client.execute(post);
		return response;
	}

	public static HttpPost setPostHeaders(HttpPost post, String soapAction) {
		post.addHeader("Authorization",
				"Basic dnRleGFwcGtleS1jYXNhYW1lcmljYS1BQkhZRVo6UUFYR1pSVllCV1hERUVSQlVDTlVQUllETEhMWElWUkFYUFFFU1RZWFNLR0dDSkxXUVVYVEFHTVpDVVVTRlhQRkVYR1BPTklCRVBDRUtSQUNXUEhPWVlaUkNSQlhZR1NVVkJDRkNXSENNV1BLRUlPS1VTV1FQQkhHWFBNWExLQ1k="); // PRODUCAO
		post.addHeader("Host", "webservice-casaamerica.vtexcommerce.com.br:8080");
		post.addHeader("SOAPAction", soapAction);
		post.addHeader("User-Agent", "Jakarta Commons-HttpClient/3.0.1");
		post.addHeader("content-type", "text/xml;charset=UTF-8");
		post.addHeader("Connection", "close");
		return post;
	}

	public static HttpPost setPostRestHeaders(HttpPost post) {
		post.addHeader("cache-control", "no-cache");
		post.addHeader("content-type", "application/json");
		post.addHeader("X-VTEX-API-AppKey", "vtexappkey-casaamerica-ABHYEZ");
		post.addHeader("X-VTEX-API-AppToken",
				"QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTYXSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY");
		return post;
	}

	public static HttpGet setHeaders(HttpGet get, int type, String token, String user) {
		if (type == 1) {
			/* VTEX */
			get.addHeader("cache-control", "no-cache");
			get.addHeader("content-type", "application/json");
			get.addHeader("X-VTEX-API-AppKey", appKey);
			get.addHeader("X-VTEX-API-AppToken", appToken);
		} else if (type == 2) {
			/* Produtos API */
			get.addHeader("content-type", "application/json");
			get.addHeader("Authorization", "Token " + token);
			get.addHeader("Login", user);
		}

		return get;
	}
}
