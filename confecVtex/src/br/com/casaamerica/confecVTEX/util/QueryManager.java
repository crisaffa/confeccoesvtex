package br.com.casaamerica.confecVTEX.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.casaamerica.confecVTEX.entities.Produto;
import br.com.casaamerica.confecVTEX.repositories.ConnectionFactoryInfat;
import br.com.casaamerica.confecVTEX.repositories.ConnectionFactoryPostgresql;

public final class QueryManager {

	private static final QueryManager INSTANCE = new QueryManager();
	Connection connectionPostgres;
	Connection connectionMysql;
	Connection connectionInfat;

	public QueryManager() {
		try {
			connectionPostgres = ConnectionFactoryPostgresql.getConnection();
			connectionInfat = ConnectionFactoryInfat.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static QueryManager getInstance() {
		return INSTANCE;
	}

	public ResultSet getIdGrupoAndNcm(Produto produto, int connectionCode) throws SQLException {
		Connection connection = this.getConnectionType(connectionCode);
		Statement statement = connection.createStatement();
		statement.execute("select id_grupo, id_ncm from produtos where id_produto = '" + produto.getId() + "' ");
		return statement.getResultSet();
	}

	public ResultSet getIdProduto(Produto produto, int connectionCode) throws SQLException {
		Connection connection = this.getConnectionType(connectionCode);
		Statement statement = connection.createStatement();
		statement
				.execute("select * from cadastroproduto where id_produto = '" + produto.getId() + produto.getPosGrade() + "'");
		return statement.getResultSet();
	}

	public ResultSet insertProduto(String id_sku, Produto produto, String linkImage, int connectionCode) throws SQLException {
		Connection connection = this.getConnectionType(connectionCode);
		Statement statement = connection.createStatement();
		statement.execute("insert into cadastroproduto (id_produto, nome, altura, "
				+ " codigo_gtin, comprimento, cor, descricao, estoque, exibir, id_grupo, id_ncm, largura, "
				+ " marca, peso, preco_promocao, preco_venda, status, imagem, nome_loja) " + "values ("
				+ Integer.parseInt(id_sku + produto.getPosGrade()) + ", '" + produto.getNome() + " "
				+ produto.getMarca() + " " + produto.getModelo() + " - " + produto.getCor() + " " + produto.getPosGrade()
				+ "', '" + produto.getAltura() + "', '" + produto.getEan() + "', '" + produto.getComprimento() + "', '"
				+ produto.getCor() + "', '" + produto.getDescricao() + "', " + produto.getEstoque() + ", 'SIM',  "
				+ produto.getIdGrupo() + ", " + produto.getIdNcm() + ", '" + produto.getLargura() + "', '" + produto.getMarca()
				+ "', '" + produto.getPeso() + "', '" + produto.getValorPromocao().toString() + "', '"
				+ produto.getPreco().toString() + "', 'PENDENTE', '" + linkImage + "', 'calcados' )");
		return statement.getResultSet();
	}

	public Connection getConnectionType(int database) {
		switch (database) {
		case 0:
			return this.connectionPostgres;
		case 1:
			return this.connectionInfat;
		default:
			return this.connectionPostgres;
		}
	}
}