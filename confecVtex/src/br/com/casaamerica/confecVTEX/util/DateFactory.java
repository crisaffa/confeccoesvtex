package br.com.casaamerica.confecVTEX.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFactory {


	public DateFactory() {

	}

	public static String getMonth(int offset) {
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH) + offset;
		String monthStr = String.valueOf(month);
		if(month < 10) {
			monthStr = "0" + monthStr;
		}
		return monthStr;
	}

	public static String getYear(int offset) {
		Calendar calendar = Calendar.getInstance();
		return String.valueOf(calendar.get(Calendar.YEAR));
	}

	public static String getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		Date data = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currenteDate = sdf.format(data);
		return currenteDate;
	}

}